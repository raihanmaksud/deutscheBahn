package rest.project.deutscheBahn.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import rest.project.deutscheBahn.model.DBObject;
import rest.project.deutscheBahn.service.csvReader;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class restController {

    public static List<DBObject> dbObjectList;
    @Autowired
    private csvReader csvReader;

    @GetMapping("/read")
    public ResponseEntity<List<DBObject>> readCsv() throws IOException {
        dbObjectList= csvReader.csvToDBObject();
        return ResponseEntity.ok(dbObjectList);
    }

    @GetMapping("/betriebsstelle/{abk}")
    public ResponseEntity<List<DBObject>> getBusinessLocation(@PathVariable String abk) {
        List<DBObject> newDBObjectList = dbObjectList
                .stream()
                .filter(dbo -> dbo.getAbk().equals(abk))
                .collect(Collectors.toList());

        return ResponseEntity.ok(newDBObjectList);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<List<DBObject>> getName(@PathVariable String name) {
        List<DBObject> newDBObjectList = dbObjectList
                .stream()
                .filter(dbo -> dbo.getName().equals(name))
                .collect(Collectors.toList());

        return ResponseEntity.ok(newDBObjectList);
    }

    @GetMapping("/kurzname/{name}")
    public ResponseEntity<List<DBObject>>  getKurzname(@PathVariable String name) {
        List<DBObject> newDBObjectList = dbObjectList
                .stream()
                .filter(dbo -> dbo.getKurzname().equals(name))
                .collect(Collectors.toList());

        return ResponseEntity.ok(newDBObjectList);
    }


}
