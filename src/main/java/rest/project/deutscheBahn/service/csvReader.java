package rest.project.deutscheBahn.service;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rest.project.deutscheBahn.model.DBObject;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


@Service
public class csvReader {


    public static List<DBObject> csvToDBObject() throws IOException {
        //String fileName = "//Users//raihanmaksud//Downloads//DBNetz-Betriebsstellenverzeichnis-Stand2017-01.csv";
        String fileName = "DBNetz-Betriebsstellenverzeichnis-Stand2017-01.csv";

        Reader reader = Files.newBufferedReader(Paths.get(fileName));

        CSVFormat csvFormat = CSVFormat.newFormat(';')
                .withFirstRecordAsHeader()
                .withHeader("Abk", "Name", "Kurzname", "Typ", "Betr-Zust", "Primary location code", "UIC", "RB", "gültig von", "gültig bis", "Netz-Key", "Fpl-rel", "Fpl-Gr");
        CSVParser csvParser = csvFormat.parse(reader);

        List<DBObject> dbObjectList = new ArrayList<>();
        Iterable<CSVRecord> csvRecords = csvParser.getRecords();
        for (CSVRecord csvRecord : csvRecords) {
            DBObject dbObject = new DBObject(
                    csvRecord.get("Abk"),
                    csvRecord.get("Name"),
                    csvRecord.get("Kurzname"),
                    csvRecord.get("Typ"),
                    csvRecord.get("Betr-Zust"),
                    csvRecord.get("Primary location code"),
                    csvRecord.get("UIC"),
                    csvRecord.get("RB"),
                    csvRecord.get("gültig von"),
                    csvRecord.get("gültig bis"),
                    csvRecord.get("Netz-Key"),
                    csvRecord.get("Fpl-rel"),
                    csvRecord.get("Fpl-Gr")
            );
            dbObjectList.add(dbObject);
        }

        return dbObjectList;

    }

}
