package rest.project.deutscheBahn.model;

import lombok.Data;


@Data
public class DBObject {

    private String abk;
    private String name;
    private String kurzname;
    private String typ;
    private String betr_zust;
    private String primary_location_code;
    private String uic;
    private String rb;
    private String gültig_von;
    private String gültig_bis;
    private String netzKey;
    private String fplRel;
    private String fplGr;

    public DBObject(String abk, String name, String kurzname, String typ, String betr_zust, String primary_location_code, String uic, String rb, String gültig_von, String gültig_bis, String netzKey, String fplRel, String fplGr) {
        this.abk = abk;
        this.name = name;
        this.kurzname = kurzname;
        this.typ = typ;
        this.betr_zust = betr_zust;
        this.primary_location_code = primary_location_code;
        this.uic = uic;
        this.rb = rb;
        this.gültig_von = gültig_von;
        this.gültig_bis = gültig_bis;
        this.netzKey = netzKey;
        this.fplRel = fplRel;
        this.fplGr = fplGr;
    }

    public DBObject(String abk, String name, String kurzname, String typ, String s, String primary_location_code, String uic, String rb, String gültig_von, String gültig_bis, String s1, String s2, String s3, String s4) {
    }
}
